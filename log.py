"""Log config"""
import logging

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    # datefmt='%Y-%m-%dT%H:%M:%S.%s+0800',
)

logger = logging.getLogger()